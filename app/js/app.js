var app = angular.module("myApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            template : "<h1>Main</h1><p>Click on the links to change this content</p>"
        })
        .when("/red", {
            templateUrl : "templates/red.html"
        })
        .when("/green", {
            templateUrl : "green.html"
        })
        .when("/blue", {
            templateUrl : "blue.html"
        });
});
