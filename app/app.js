'use strict';

angular.module('myApp', ["ngRoute"])
.config(function($routeProvider) {
  $routeProvider
      .when("/", {
        templateUrl : "view/persons/all.html"
      })
      .when("/new", {
        templateUrl : "view/persons/add.html"
      })
      .when("/edit", {
        templateUrl : "view/persons/edit.html"
      })
      .when("/show", {
        templateUrl : "view/persons/show.html"
      })
      .when("/delete", {
          templateUrl : "view/persons/delete.html"
      })
      .when("/select", {
          templateUrl : "view/persons/extsel.html"
      })
      .otherwise({
        template : "<h3>Tamplate doesn't exist.</h3>"
      });
})
.controller('topCtrl', function($scope) {
    //console.log('topCtrl');
   var q;
   $scope.count_front = $scope.count_back = $scope.count_new = $scope.count_reserve = $scope.count_refused =
       $scope.count_yes = $scope.count_no = $scope.count_consider = 0;
    $scope.newPerson = {
        name: "",
        skill: "",
        dateOfBirth: "",
        city: "",
        phone: "",
        skype: "",
        salary: "",
        age: "",
        email: ""
    };

  $scope.sortEl = 'name';

  $scope.persons = [{name:"Vasya", skill:"sk1", dateOfBirth:"18 02 1980", city:"London", phone:55555, skype:"asdfg",
                     salary:7777, age:36, email:'a@a.com', status: [], proffession: []},
                  {name:"Kolya", skill:"sk2", dateOfBirth:"15 06 1982", city:"Kyiv", phone:33333, skype:"gdfgh",
                      salary:5555, age:34, email:'b@b.com', status: [], proffession: []},
                  {name:"Dima", skill:"sk3", dateOfBirth:"05 10 1977", city:"Kalush", phone:11111, skype:"df3gh",
                      salary:8888, age:39, email:'c@c.com', status: [], proffession: []},
                  {name:"Petya", skill:"sk4", dateOfBirth:"22 08 1978", city:"Lviv", phone:22222, skype:"rtdf3gh",
                      salary:7888, age:38, email:'d@d.com', status: [], proffession: []},
                  {name:"Ihor", skill:"sk5", dateOfBirth:"01 12 1983", city:"Iv-Fr", phone:77777, skype:"asdf3gh",
                      salary:9888, age:33, email:'e@e.com', status: [], proffession: []}];

  for (var i in $scope.persons) { $scope.persons[i].id = parseInt(i) + 1; };

  var countFunc = function() {
    $scope.count_front = $scope.count_back = $scope.count_new = $scope.count_reserve = $scope.count_refused =
          $scope.count_yes = $scope.count_no = $scope.count_consider = 0;
    for (var i in $scope.persons) {
        if (($scope.persons[i].status).indexOf('New') >= 0)      $scope.count_new += 1;
        if (($scope.persons[i].status).indexOf('Reserve') >= 0)  $scope.count_reserve += 1;
        if (($scope.persons[i].status).indexOf('Refused') >= 0)  $scope.count_refused += 1;
        if (($scope.persons[i].status).indexOf('Yes') >= 0)      $scope.count_yes += 1;
        if (($scope.persons[i].status).indexOf('No') >= 0)       $scope.count_no += 1;
        if (($scope.persons[i].status).indexOf('Consider') >= 0) $scope.count_consider += 1;

        if (($scope.persons[i].proffession).indexOf('Front-end') >= 0) $scope.count_front += 1;
        if (($scope.persons[i].proffession).indexOf('Back-end') >= 0)  $scope.count_back += 1;
    }
  };

  countFunc();

  $scope.counter = $scope.persons.length;
  $scope.queryPerson = "";
  $scope.searchAllow = false;
  $scope.searchVal = "Search";


  $scope.searchFunc = function() {
    $scope.searchAllow = !($scope.searchAllow);
    $scope.searchVal = ($scope.searchAllow) ? "Hide Search" : "Search";
    $scope.queryPerson = "";
    console.log("$scope.searchAllow - " + $scope.searchAllow );
  };


  $scope.removeResume = function(x) {
    var name_del, id_del, i;
    for (i in $scope.persons) { if ($scope.persons[i].id == x) {name_del = $scope.persons[i].name; id_del = i;} }
    if (confirm("Are You Sure (delete " + name_del + ")?")) {
      $scope.persons.splice(id_del, 1);
      $scope.counter = $scope.persons.length;
      $scope.queryPerson = "";
    }
    countFunc();
  };


  $scope.editResume = function(x) {
      var proffessions = document.getElementsByName("editProff"), arr_proffessions = [], i;
      for (i in proffessions) { if (proffessions[i].checked)  arr_proffessions.push(proffessions[i].value);  }

      var status = document.getElementsByName("editStatus"), arr_status = [];
      for (i in status) { if (status[i].checked)  arr_status.push(status[i].value); }
      //console.log('arr_proffessions - ' + arr_proffessions);
      //console.log('arr_status - ' + arr_status);

      for (i in $scope.persons) {
          if ($scope.persons[i].id == x) {
              $scope.persons[i].name        = $scope.person.name;
              $scope.persons[i].age         = parseInt($scope.person.age);
              $scope.persons[i].skill       = $scope.person.skill;
              $scope.persons[i].email       = $scope.person.email;
              $scope.persons[i].dateOfBirth = $scope.person.dateOfBirth;
              $scope.persons[i].salary      = parseInt($scope.person.salary);
              $scope.persons[i].skype       = $scope.person.skype;
              $scope.persons[i].phone       = parseInt($scope.person.phone);
              $scope.persons[i].city        = $scope.person.city;
              $scope.persons[i].proffession = arr_proffessions;
              $scope.persons[i].status      = arr_status;
              //console.log('$scope.persons[i].proffession - ' + $scope.persons[i].proffession);
              //console.log('$scope.persons[i].status - ' + $scope.persons[i].status);
          }
      }
      countFunc();
  };


  $scope.showResume = function(x) {
    for (var i in $scope.persons) { if ($scope.persons[i].id == x) $scope.person = $scope.persons[i]; }
  };


  $scope.preEditResume = function(x) {
    var i, j;
      $scope.checkboxProff = [
          { name: 'Front-end', selected: false },
          { name: 'Back-end',  selected: false }
      ];

      $scope.checkboxStatus = [
          { name: 'Нове',               id: 'New',      selected: false },
          { name: 'В розгляді',         id: 'Consider', selected: false },
          { name: 'Пройшов співбесіду', id: 'Yes',      selected: false },
          { name: 'Резерв',             id: 'Reserve',  selected: false },
          { name: 'Відмовився',         id: 'Refused',  selected: false },
          { name: 'Не підходить',       id: 'No',       selected: false }
      ];

    for (i in $scope.persons) { if ($scope.persons[i].id == x) $scope.person = $scope.persons[i]; }
    
    for (i in $scope.person.status) { //console.log('$scope.person.status - '+$scope.person.status[i]);
        for (j in $scope.checkboxStatus) {
            if ($scope.person.status[i] == $scope.checkboxStatus[j].id) {
                $scope.checkboxStatus[j].selected = true;
            }
        }
    };

    for (i in $scope.person.proffession) { //console.log('$scope.person.proffession - ' + $scope.person.proffession[i]);
       for (j in $scope.checkboxProff) { //console.log('personProffession - '+personProffession[j].value);
          if ($scope.person.proffession[i] == $scope.checkboxProff[j].name) {
              $scope.checkboxProff[j].selected = true;
              //console.log($scope.checkboxProff[j].name + ' - ' + $scope.checkboxProff[j].selected);
                }
          }
    }
  };


  $scope.allFunc = function() {
    $scope.queryPerson = "";
  };


  $scope.newResume = function() {
      var proffessions = document.getElementsByName("proffession"), arr_proffessions = [], i;
      for (i in proffessions) { if (proffessions[i].checked)  arr_proffessions.push(proffessions[i].value);  }

      var status = document.getElementsByName("status"), arr_status = [];
      for (i in status) { if (status[i].checked)  arr_status.push(status[i].value); }

      $scope.persons.push({
          name: $scope.newPerson.name,
          age: $scope.newPerson.age,
          email: $scope.newPerson.email,
          skill: $scope.newPerson.skill,
          dateOfBirth: $scope.newPerson.dateOfBirth,
          salary: $scope.newPerson.salary,
          skype: $scope.newPerson.skype,
          phone: $scope.newPerson.phone,
          city: $scope.newPerson.city,
          proffession: arr_proffessions,
          status: arr_status,
          id: ($scope.persons[$scope.persons.length - 1].id + 1)
      });
      $scope.newPerson = {};
      $scope.counter = $scope.persons.length;
      countFunc();
  };


  $scope.extSelect = function() {
    $scope.queryPerson = "";
    var proff = document.getElementById("proff").value,
        status = document.getElementById("status").value,
        i;
    $scope.selPersons = Object.create($scope.persons);
    //console.log('$scope.selPersons - ' + $scope.selPersons.length);
    for (i=0;i<$scope.selPersons.length;i++) {
      if ( ($scope.selPersons[i].proffession).indexOf(proff) < 0 || ($scope.selPersons[i].status).indexOf(status) < 0 )
        { $scope.selPersons.splice(i, 1); --i; }
    }
    //console.log('$scope.selPersons - ' + $scope.selPersons.length);
  };

  $scope.settings = function(col) {
    $scope.queryPerson = "";
    document.getElementsByTagName("body")[0].className = col;
  };


});
